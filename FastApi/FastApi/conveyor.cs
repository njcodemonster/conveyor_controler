﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.IO;
using System.Net;
using System.Web;
using System.Collections.Concurrent;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Management;
using System.IO.Ports;
using System.Threading;
using FastApi;

namespace FastApi
{
    class conveyor
    {
        public String orderID = "";
        private static SerialPort conveyorConnection = new SerialPort();
        private enum conveyerstatus
        {
            busy = 2,
            stoperd = 1,
            waiting = 0,
            bottmFetching ,
            topFetching,
            cutomeRequest,
            notconnected

        };
        private Items currentItem;
        private static object lockerSerial = new object();
        public  String status = conveyerstatus.notconnected.ToString();
        public  bool CloseSerial()
        {
            try
            {
                Console.WriteLine("Closing COM1...");
                conveyorConnection.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Error closing COM1");
                status = conveyerstatus.notconnected.ToString();
                return false;
            }
            Console.WriteLine("Close COM1 Successful.");
            status = conveyerstatus.notconnected.ToString();
            return true;
        }
        private  bool ConnectSerial()
        {
            if (conveyorConnection.IsOpen)
            {
                return true;
            }
            try
            {
                Console.WriteLine("Opening COM1...");
                string selectedPort = "";
                var ports = COMPortInfo.GetCOMPortsInfo();
                foreach (COMPortInfo comPort in ports)
                {
                    if (comPort.Description.Contains("Communications Port"))
                    {
                        selectedPort = comPort.Name;
                        break;
                    }
                }


                conveyorConnection.NewLine = "\r\n";
                conveyorConnection.PortName = selectedPort;
                conveyorConnection.BaudRate = 19200;
                conveyorConnection.Parity = Parity.None;
                conveyorConnection.DataBits = 8;
                conveyorConnection.StopBits = StopBits.One;
                conveyorConnection.Handshake = Handshake.None;
                conveyorConnection.DtrEnable = true;
                conveyorConnection.ReadTimeout = 5000;
                conveyorConnection.WriteBufferSize = 1024;

                conveyorConnection.Open();
            }
            catch (Exception)
            {
                Console.WriteLine("Error opening COM1!");
                return false;
            }

            Console.WriteLine("Open COM1 Successful.");
            status = conveyerstatus.waiting.ToString();
            return true;
        }
        private String stop ()
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                try
                {
                    ConnectSerial();
                    WriteCommand("$0SSTOP");
                    string response = ReadLine();
                    CloseSerial();
                    return response;
                }
                catch (Exception ee)
                {
                    return ee.Message;
                }
            }

           // ctx.SendReply("FAIL");
        }
        private static bool WriteCommand(string cmd)
        {
            try
            {
                Console.WriteLine("Writing COM1...");
                conveyorConnection.BaseStream.Flush();
                conveyorConnection.WriteLine(cmd);
                conveyorConnection.BaseStream.Flush();
            }
            catch (Exception)
            {
                Console.WriteLine("Error writing COM1");
                return false;
            }
            Console.WriteLine("Write COM1 Successful.");
            return true;
        }

        private  string ReadLine()
        {
            try
            {
                Console.WriteLine("Reading COM1...");

                StringBuilder sb = new StringBuilder();
                char rdChar = (char)0;
                while ((rdChar = (char)conveyorConnection.ReadByte()) != '\r')
                sb.Append(rdChar);
                sb.Append(rdChar);
                Console.WriteLine("Read COM1 Successful.");
                return sb.ToString();
            }
            catch (Exception)
            {
               
                Console.WriteLine("Error writing COM1");
                return "";
            }
        }
       
        
        public Boolean forceStop()
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                status = conveyerstatus.busy.ToString();
                try
                {
                    ConnectSerial();
                    WriteCommand("$0SSTOP");
                    string response = ReadLine();
                    //CloseSerial();
                    status = conveyerstatus.waiting.ToString();
                    return true;
                }
                catch (Exception ee)
                {
                    status = conveyerstatus.waiting.ToString();
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                    return false;
                }
            }

          
        }
        public Int32 getPos(Boolean bottom)
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                try
                {
                    ConnectSerial();
                    if (bottom)
                    {
                        WriteCommand("$0R1");
                    }
                    else
                    {
                        WriteCommand("$0R2");
                    }
                   
                    string response = ReadLine();
                    //CloseSerial();
                    Int32 currentPos;
                    try
                    {
                        currentPos = Convert.ToInt32(response.Split(',')[1]);
                    }
                    catch (Exception ex)
                    {
                        currentPos = 1;
                    }
                    return currentPos;
                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                    return 1;
                }
            }


        }
        
        public Boolean getItem(Int32 slot,Boolean bottom)
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                status = conveyerstatus.busy.ToString();
                try
                {
                    ConnectSerial();
                    if (bottom)
                    {
                        WriteCommand("$0G" + slot.ToString());
                    }
                    else
                    {
                        WriteCommand("$0G" + "00" + "," + slot.ToString());
                    }
                   
                    string response = ReadLine();
                    //CloseSerial();
                    if (response.ToLower().Contains("ack"))
                    {
                        status = conveyerstatus.waiting.ToString();
                        return true;
                    }
                    else
                    {
                        status = conveyerstatus.waiting.ToString();
                        return false;
                    }
                  
                    
                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                    status = conveyerstatus.waiting.ToString();
                    return false;
                }
            }

           
        }

    }
}
