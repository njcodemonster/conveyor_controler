﻿using System;
using WebSocketSharp;
using System.Text;
using System.Threading;

namespace FastApi
{
    class routerClient
    {
       //System.Web.WebSockets.AspNetWebSocket broadcaster = new AspNetWebSocket();
        private WebSocket client = null;

       const string host = "wss://hautedb.com:9999";
        public   void connect()
        {
            if (client != null && client.ReadyState == WebSocketState.Open)
            {
                return;
            }
            client = new WebSocket(host);
            client.Connect();
        }
        public void free()
        {
            
            send_update("free");
        }
        public void bussy()
        {
            send_update("busy");
        }
        private async void send_update(String status)
        {
            Console.WriteLine(status);
            connect();
            client.Send(status);
        }
    }
}
