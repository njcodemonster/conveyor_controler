﻿namespace AuthBuffer
{
    using AuthBuffer.Http;
    using AuthBuffer.Http_Skillyvitus;
    using System.Web.Script.Serialization;
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Collections.Concurrent;
    using System.Text;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Management;
    using System.IO.Ports;
    using System.Threading;
    using FastApi;

    internal class Program
    {
        private  enum fetchstatus
        {
            bottomended,
            topended
        };
        private static readonly HttpRequestDispatcher Dispatcher = new HttpRequestDispatcher();
        static Boolean bottomNeeded = false;
        private static string returnValue = "reply_from_fastapi";
        private static bestPath slotsAndPaths = new bestPath();
        static Boolean fetchingOrder = false;
        private static conveyor conveyorControler = new conveyor();
        private static object lockerPut = new object();
        public  Int32 bottomIndex = 0;
        public Int32 topIndex = 0;
        private static routerClient rt = new routerClient();
        //public static Boolean bottomEnd = false;
        //public static Boolean topEnd = false;

        private static void Main(string[] args)
        {
            rt.connect();
            /* List<Items> slotsAll = new List<Items>();
             for (Int32 i = 0; i < 1200; i++)
             {
                 var it = new Items();
                 it.slotNumber = i;
                 it.barcodeNumber = i;
                 it.sku = "skusku" + i.ToString();
                 slotsAll.Add(it);
             }
            bestPath bp = new bestPath();
            bp.init(slotsAll);
            bp.generatePathBottom(30);
            foreach (Items a in bp.pathBottom)
            {
                Console.WriteLine(a.barcodeNumber);
            }
            Console.WriteLine("TOP");
            bp.generatePathTop(301);
            foreach (Items a in bp.pathTop)
            {
                Console.WriteLine(a.barcodeNumber);
            }
            Console.ReadLine();*/
          //  Dispatcher.DynamicRoutes.Add("test/put", new Action<HttpRequestContext>(Program.HandleTestPut));
           // Dispatcher.DynamicRoutes.Add("test/get", new Action<HttpRequestContext>(Program.HandleTestGet));
           
            Dispatcher.DynamicRoutes.Add("conveyor/getCurrentPosBottom", new Action<HttpRequestContext>(Program.HandleConveyerPosBottom));
            Dispatcher.DynamicRoutes.Add("conveyor/getCurrentPosTop", new Action<HttpRequestContext>(Program.HandleConveyerPosTop));
            Dispatcher.DynamicRoutes.Add("conveyor/fetchNext", new Action<HttpRequestContext>(Program.HandleFetchNext));
            Dispatcher.DynamicRoutes.Add("conveyor/topforward", new Action<HttpRequestContext>(Program.HandleTopForward));
            Dispatcher.DynamicRoutes.Add("conveyor/topbackward", new Action<HttpRequestContext>(Program.HandleTopBackward));
            Dispatcher.DynamicRoutes.Add("conveyor/bottomforward", new Action<HttpRequestContext>(Program.HandleBottomForward));
            Dispatcher.DynamicRoutes.Add("conveyor/bottombackward", new Action<HttpRequestContext>(Program.HandleBottomBackwward));
            Dispatcher.DynamicRoutes.Add("conveyor/free", new Action<HttpRequestContext>(Program.HandleFreeConveyor));
            Dispatcher.DynamicRoutes.Add("conveyor/cancel", new Action<HttpRequestContext>(Program.HandleCancle));
            Dispatcher.DynamicRoutes.Add("conveyor/skip", new Action<HttpRequestContext>(Program.HandleSkip));
           // Dispatcher.DynamicRoutes.Add("conveyer/getCurrentStatus", new Action<HttpRequestContext>(Program.HandleConveryerstatus));
            //Dispatcher.DynamicRoutes.Add("conveyer/moveForward", new Action<HttpRequestContext>(Program.HandleConveyerForward));
            //Dispatcher.DynamicRoutes.Add("conveyer/moveBackward", new Action<HttpRequestContext>(Program.HandleConveyerBackwards));
            //keep this here, some browsers always ping favicon.ico to see if site is up
            //Dispatcher.DynamicRoutes.Add("favicon.ico", new Action<HttpRequestContext>(Program.HandleFavicon));
            Dispatcher.DynamicRoutes.Add("conveyor/fetchList", new Action<HttpRequestContext>(Program.HandleFetchList));
            Dispatcher.DynamicRoutes.Add("conveyor/status", new Action<HttpRequestContext>(Program.HandleGetStatus));
            Dispatcher.Run();
            Console.WriteLine("quit, save, reload");
            rt.free();
            string text = "";
            while ((text = Console.ReadLine()) != "quit")
            {
                if (text == "save")
                {
                    //do something
                }
                if (text == "reload")
                {
                    //do something
                }
                if (text == "connect")
                {
                    //do something
                }
            }

            Dispatcher.Stop();
            conveyorControler.CloseSerial();
        }


        private static void HandleFreeConveyor(HttpRequestContext ctx)
        {
            rt.free();
            ctx.SendReply("ok");
        }
        private static void HandleGetStatus(HttpRequestContext ctx)
        {
            try
            {
                rt.bussy();
                if (slotsAndPaths.currentlyFetching == null)
                {
                    ctx.SendReply("fechingorder,convstatus,bottomended,topended,currentbc,bottomindex,topindex--" + fetchingOrder.ToString() + "," + conveyorControler.status + "," + slotsAndPaths.bottomEnd.ToString() + "," + slotsAndPaths.topEnd.ToString() + "," + "--" + "," + slotsAndPaths.bottomIndex.ToString() + "," + slotsAndPaths.topIndx.ToString());
                }
                else
                {
                    ctx.SendReply("fechingorder,convstatus,bottomended,topended,currentbc,bottomindex,topindex--" + fetchingOrder.ToString() + "," + conveyorControler.status + "," + slotsAndPaths.bottomEnd.ToString() + "," + slotsAndPaths.topEnd.ToString() + "," + slotsAndPaths.currentlyFetching.barcodeNumber + "," + slotsAndPaths.bottomIndex.ToString() + "," + slotsAndPaths.topIndx.ToString());
                }

               // rt.free();    
            }
            catch (Exception ex)
            {
               // rt.free();
                ctx.SendReply("something wrong happened" + ex.Message);
            }
        }

        private static void HandleFetchNext(HttpRequestContext ctx)
        {
            rt.bussy();
            try
            {
                String fetchingFailed = "";
                String returnstring = "";
                if (bottomNeeded && !slotsAndPaths.bottomEnd)
                {
                    if (!slotsAndPaths.fetchNextBottom(conveyorControler))
                    {
                        fetchingFailed = "failed to fetch the item" + slotsAndPaths.currentlyFetching.barcodeNumber;
                    }
                    else
                    {
                        fetchingFailed = "fetching" + slotsAndPaths.currentlyFetching.barcodeNumber + "," + slotsAndPaths.currentlyFetching.orderId; 
                    }
                    if (slotsAndPaths.bottomEnd)
                    {
                        returnstring = returnstring + "," + fetchstatus.bottomended.ToString() ;
                    }
                }
                else
                {
                    slotsAndPaths.bottomEnd = true;
                    returnstring = returnstring + "," + fetchstatus.bottomended.ToString();
                    if (!slotsAndPaths.fetchNextTop(conveyorControler))
                    {
                        fetchingFailed = "failed to fetch the item" + slotsAndPaths.currentlyFetching.barcodeNumber;
                    }
                    else
                    {
                        fetchingFailed = "fetching" + slotsAndPaths.currentlyFetching.barcodeNumber + "," + slotsAndPaths.currentlyFetching.orderId; 
                    }
                    if (slotsAndPaths.topEnd)
                    {
                        returnstring = returnstring + "," + fetchstatus.topended.ToString();
                    }

                }
                returnstring=returnstring + ",bottomindex:" + slotsAndPaths.bottomIndex + ",topindex:" + slotsAndPaths.topIndx;
                ctx.SendReply(returnstring + "," + fetchingFailed);
              //  rt.free();
            }
            catch (Exception ex)
            {
             //   rt.free();
            }
        }
        private static void HandleSkip(HttpRequestContext ctx)
        {

          
            ctx.SendReply(conveyorControler.forceStop().ToString());
           
        }
        private static void HandleCancle(HttpRequestContext ctx)
        {
           
            slotsAndPaths.bottomIndex = 0;
            slotsAndPaths.topIndx = 0;
            slotsAndPaths.bottomEnd = false;
            slotsAndPaths.topEnd = false;
            fetchingOrder = false;
            ctx.SendReply(conveyorControler.forceStop().ToString());
            rt.free();
        }
        private static void HandleFetchList(HttpRequestContext ctx)
        {
            rt.bussy();
            String returnstring = "";
            slotsAndPaths.bottomIndex = 0;
            slotsAndPaths.topIndx = 0;
            slotsAndPaths.bottomEnd = false;
            slotsAndPaths.topEnd = false;
            fetchingOrder = false;
            String Jsonstring ="";
            Jsonstring = ctx.RequestBody;
            String orderID = "";
            String fetchingFailed = "";
            try
            {
                ctx.Parameters.TryGetValue("order", out orderID);
            }
            catch (Exception ex)
            {
            }
            if (orderID != "")
            {
                fetchingOrder = true;
                conveyorControler.orderID = orderID;
            }
            try
            {
                var ser = new JavaScriptSerializer();
                Dictionary<string, object> ItemsJson = (Dictionary<string, object>)ser.DeserializeObject(Jsonstring);
                object[] itemsList = (object[])ItemsJson["items"];
                List<Items> slotsAll = new List<Items>();
                foreach (object baseItem in itemsList)
                {
                    Dictionary<string, object> Item = (Dictionary<string, object>)baseItem;
                    Items listItem = new Items();
                    try
                    {
                    listItem.slotNumber =(Int32) Item["slot"];
                    listItem.barcodeNumber = (Int32)Item["barcode"];
                    listItem.sku = (String)Item["sku"];
                    listItem.productId = (String)Item["productid"];
                    listItem.orderId = (String)Item["orderid"];
                    }
                    catch (Exception ex)
                    {
                        listItem.orderId = "";
                    }
                    slotsAll.Add(listItem);
                   
                }
                slotsAndPaths.init(slotsAll);
                slotsAndPaths.generatePathBottom(conveyorControler.getPos(true));
                slotsAndPaths.generatePathTop(conveyorControler.getPos(false));
                returnstring =  slotsAndPaths.pathBottom.Count.ToString() + "," + slotsAndPaths.pathTop.Count.ToString();
                if (slotsAndPaths.pathBottom.Count > 0)
                {
                    bottomNeeded = true;
                }
                else
                {
                    bottomNeeded = false;
                }
                if (bottomNeeded)
                {
                    if (!slotsAndPaths.fetchNextBottom(conveyorControler))
                    {
                        fetchingFailed = "failed to fetch the item" + slotsAndPaths.currentlyFetching.barcodeNumber;
                    }
                    else
                    {
                        fetchingFailed = "fetching" + slotsAndPaths.currentlyFetching.barcodeNumber+","+ slotsAndPaths.currentlyFetching.orderId;
                    }
                    if (slotsAndPaths.bottomEnd)
                    {
                        returnstring = returnstring + "," + fetchstatus.bottomended.ToString();
                    }
                    if(slotsAndPaths.pathTop.Count>0){
                        slotsAndPaths.fetchNextTop(conveyorControler);
                        slotsAndPaths.topIndx--;
                    }
                }
                else
                {
                    slotsAndPaths.bottomEnd = true;
                    returnstring = returnstring + "," + fetchstatus.bottomended.ToString();
                    if (!slotsAndPaths.fetchNextTop(conveyorControler))
                    {
                        fetchingFailed = "failed to fetch the item" + slotsAndPaths.currentlyFetching.barcodeNumber;
                    }
                    else
                    {
                        fetchingFailed = "fetching" + slotsAndPaths.currentlyFetching.barcodeNumber + "," + slotsAndPaths.currentlyFetching.orderId; 
                    }
                    if (slotsAndPaths.topEnd)
                    {
                        returnstring = returnstring + "," + fetchstatus.topended.ToString();
                    }

                }
                returnstring = returnstring + ",bottomindex:" + slotsAndPaths.bottomIndex + ",topindex:" + slotsAndPaths.topIndx;
                ctx.SendReply(returnstring+","+fetchingFailed);
               // System.Threading.Thread.Sleep(9000);
               // rt.free();
            }
            catch (Exception ex)
            {
             //   rt.free();
                ctx.SendReply("something wrong happened"+ex.Message);
                
            }
        }
        private static void HandleTopForward(HttpRequestContext ctx){
           rt.bussy();
           try
           {
               Int32 currentPos = 0;
               currentPos = conveyorControler.getPos(false);
               conveyorControler.getItem(currentPos + 10, false);
               rt.free();
           }
           catch (Exception ex)
           {
               rt.free();
           }
        }
        private static void HandleTopBackward(HttpRequestContext ctx){
           rt.bussy();
           try
           {
               Int32 currentPos = 2;
               currentPos = conveyorControler.getPos(false);
               conveyorControler.getItem(currentPos - 10, false);
               rt.free();
           }
           catch (Exception ex)
           {
               rt.free();
           }
        }
        private static void HandleBottomForward(HttpRequestContext ctx){
            rt.bussy();
            try
            {
                Int32 currentPos = 0;
                currentPos = conveyorControler.getPos(true);
                conveyorControler.getItem(currentPos + 10, true);
                rt.free();
            }
            catch (Exception ex)
            {
                rt.free();
            }

        }
         private static void HandleBottomBackwward(HttpRequestContext ctx){
             rt.bussy();
             try
             {
                 Int32 currentPos = 2;
                 currentPos = conveyorControler.getPos(true);
                 conveyorControler.getItem(currentPos - 10, true);
                 rt.free();
             }
             catch (Exception ex)
             {
                 rt.free();
             }

        }

        private static void HandleConveyerPosBottom(HttpRequestContext ctx)
        {
            // rt.bussy();
            try
            {
                ctx.SendReply(conveyorControler.getPos(true).ToString());
                //rt.free();
            }
            catch (Exception ex)
            {
               // rt.free();
            }
        }
        private static void HandleConveyerPosTop(HttpRequestContext ctx)
        {
           // rt.bussy();
            try
            {
                ctx.SendReply(conveyorControler.getPos(false).ToString());
              //  rt.free();
            }
            catch (Exception ex)
            {
               // rt.free();
            }
        }
       /* private static void HandleConveyerPos(HttpRequestContext ctx)
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                try
                {
                    ConnectSerial();
                    WriteCommand("$0R2");
                    string response = ReadLine();
                    CloseSerial();
                    ctx.SendReply(response);
                    return;
                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                }
            }

            ctx.SendReply("FAIL");
        }

        private static void HandleConveyerForward(HttpRequestContext ctx)
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                try
                {
                    ConnectSerial();
                    WriteCommand("$0R2");
                    string currentpos = ReadLine();
                    Console.WriteLine(currentpos.Split(',')[1]);
                    Int32 newpos = Convert.ToInt32(currentpos.Split(',')[1]) + 1;
                    string newPos = newpos.ToString();
                   // WriteCommand("$2G"+newPos);
                   // string response = ReadLine();
                    //ctx.SendReply(response);
                    WriteCommand("$0G" +"11"+","+"122");
                    string response = ReadLine();
                    ctx.SendReply(response);
                    CloseSerial();
                    ctx.SendReply(response);
                    return;
                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                }
            }

            ctx.SendReply("FAIL");
        }

        private static void HandleConveyerBackwards(HttpRequestContext ctx)
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                try
                {
                    ConnectSerial();
                    WriteCommand("$0R1");
                    string currentpos = ReadLine();
                    Console.WriteLine(currentpos.Split(',')[1]);
                    Int32 newpos = Convert.ToInt32(currentpos.Split(',')[1]) - 1;
                    string newPos = newpos.ToString();
                    WriteCommand("$0G" + newPos);
                    string response = ReadLine();
                    CloseSerial();
                    ctx.SendReply(response);
                    return;
                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                }
            }

            ctx.SendReply("FAIL");
        }

        private static void HandleConveryerstatus(HttpRequestContext ctx)
        {
            ctx.SendReply(conveyerstatus.waiting.ToString());
        }

        private static void HandleFavicon(HttpRequestContext ctx)
        {
            ctx.SendReply("OK");
        }

        private static void HandleTestGet(HttpRequestContext ctx)
        {
            ctx.SendReply("OK");
        }
        private static void HandleTestPut(HttpRequestContext ctx)
        {
            string name = "";

            if (ctx.Parameters.TryGetValue("name", out name))
            {
                try
                {

                    lock (lockerPut) //only one thread can write to memory at a time
                    {
                        returnValue = name;
                    }

                    ctx.SendReply("OK");
                    return;
                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                }
            }

            ctx.SendReply("FAIL");
        }

        private static void HandleConveyorStop(HttpRequestContext ctx)
        {
            lock (lockerSerial)  //always lock this before doing serial stuff because http requests are coming in on multiple threads 
            {
                try
                {
                    ConnectSerial();
                    WriteCommand("$0SSTOP");
                    string response = ReadLine();
                    CloseSerial();
                    ctx.SendReply(response);
                    return;
                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
                }
            }

            ctx.SendReply("FAIL");
        }*/

       /* private static void HandleTestGet(HttpRequestContext ctx)
        {
            try
            {
                string param = "";
                if (ctx.Parameters.ContainsKey("naveed"))
                    param += ctx.Parameters["naveed"];

                ctx.SendReply("OK|" + returnValue + "|" + param);
                return;
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.Message + ":\n" + ee.StackTrace);
            }

            ctx.SendReply("FAIL");
        }*/
    }
}

