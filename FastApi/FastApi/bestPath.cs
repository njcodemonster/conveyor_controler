﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FastApi;

namespace FastApi
{
    class bestPath
    {
        private Int32 bottomLimit = 1700;
        private Int32 topFirstLevelMax = 3610;
        private List<Items> slotsAll = new List<Items>();
        public List<Items> pathBottom = new List<Items>();
        public List<Items> pathTop = new List<Items>();
        private List<Items> slotsBottom = new List<Items>();
        private List<Items> slotsTop = new List<Items>();
        public Int32 bottomIndex = 0;
        public Int32 topIndx = 0;
        public Boolean bottomEnd = false;
        public Boolean topEnd = false;
        public Items currentlyFetching = new Items();
        private static int CompareDinosBySlot(Items x, Items y)
        {
            if (x.slotNumber > y.slotNumber)
            {
                return 1;
            }
            if(x.slotNumber <y.slotNumber){
                return -1;
            }
            else
            {
                return 0;
            }
        }

        private static int CompareDinosBySlotreverse(Items x, Items y)
        {
            if (x.slotNumber > y.slotNumber)
            {
                return -1;
            }
            if (x.slotNumber < y.slotNumber)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public void init(List<Items> _slots)
        {
            slotsBottom.RemoveRange(0, slotsBottom.Count);
            slotsTop.RemoveRange(0, slotsTop.Count);
            pathBottom.RemoveRange(0, pathBottom.Count);
            pathTop.RemoveRange(0, pathTop.Count);
            slotsAll = _slots;

            foreach (Items CurrentSlot in _slots)
            {
                if (CurrentSlot.slotNumber <= 1700)
                {
                    slotsBottom.Add(CurrentSlot);
                }
                else
                {
                    if (CurrentSlot.slotNumber > 3610)
                    {
                        CurrentSlot.slotNumber = CurrentSlot.slotNumber - 1910;
                        slotsTop.Add(CurrentSlot);
                    }
                    else
                    {
                        slotsTop.Add(CurrentSlot);
                    }
                }
            }
        }
       
        public void generatePathBottom(Int32 currentPosBottom)
        {
            if (slotsBottom.Count < 1)
            {
                bottomEnd = true;
                return;
            }
            List<Items> _slotsBottom =  slotsBottom;
            List<Items> _slotsBottom_ubove = new List<Items>(); 
            List<Items> _slotsBottom_under = new List<Items>(); 
            List<Items> _slotsBottom_same = new List<Items>();
            pathBottom.RemoveRange(0, pathBottom.Count);
            Items candidate = new Items();
            Int32 LastDistance = 1000000;
            Int32 undermax = 0;
            Int32 abovemax = 0;
            Int32 newDistance = 0;
            Int32 indextoremove = 0;
            Int32 itrativeIndex = 0;
            foreach (Items CurrentSlot in _slotsBottom)
            {
                if (currentPosBottom - CurrentSlot.slotNumber < 0)
                {
                    _slotsBottom_ubove.Add(CurrentSlot);
                    abovemax = (currentPosBottom - CurrentSlot.slotNumber) * -1;
                }
                if(currentPosBottom - CurrentSlot.slotNumber > 0)
                {
                    _slotsBottom_under.Add(CurrentSlot);
                    undermax = currentPosBottom - CurrentSlot.slotNumber;
                }
                if (currentPosBottom - CurrentSlot.slotNumber == 0)
                {
                    _slotsBottom_same.Add(CurrentSlot);
                }
            }
            _slotsBottom_same.Sort(CompareDinosBySlot);
            _slotsBottom_ubove.Sort(CompareDinosBySlot);
            _slotsBottom_under.Sort(CompareDinosBySlotreverse);
            if (undermax < abovemax)
            {
                if (_slotsBottom_same.Count > 0)
                {
                    pathBottom.AddRange(_slotsBottom_same);
                }
                if (_slotsBottom_under.Count > 0)
                {
                    pathBottom.AddRange(_slotsBottom_under);
                }
                if (_slotsBottom_ubove.Count > 0)
                {
                    pathBottom.AddRange(_slotsBottom_ubove);
                }
            }
            else
            {
                if (_slotsBottom_same.Count > 0)
                {
                    pathBottom.AddRange(_slotsBottom_same);
                }
                if (_slotsBottom_ubove.Count > 0)
                {
                    pathBottom.AddRange(_slotsBottom_ubove);
                }
                if (_slotsBottom_under.Count > 0)
                {
                    pathBottom.AddRange(_slotsBottom_under);
                }
               
            }
          
        }

        public void generatePathTop(Int32 currentPosTop)
        {
            if (slotsTop.Count() == 0)
            {
                topEnd = true;
                return;
            }
            List<Items> _slotsTop_ubove = new List<Items>();
            List<Items> _slotsTop_under = new List<Items>();
            List<Items> _slotsTop_same = new List<Items>();
            List<Items> _slotsTop = slotsTop;
            Int32 abovemax = 0;
            Int32 undermax = 0;
            pathBottom.RemoveRange(0,  pathTop.Count());
            Items candidate = new Items();
            Int32 LastDistance = 1000000;
            Int32 newDistance = 0;
            Int32 lastRemoved = 1100000;
            Int32 indextoremove = 0;
            Int32 itrativeIndex = 0;
            foreach (Items CurrentSlot in _slotsTop)
            {
                if (currentPosTop - CurrentSlot.slotNumber < 0)
                {
                    _slotsTop_ubove.Add(CurrentSlot);
                    abovemax = (currentPosTop - CurrentSlot.slotNumber) * -1;
                }
                if (currentPosTop - CurrentSlot.slotNumber > 0)
                {
                    _slotsTop_under.Add(CurrentSlot);
                    undermax = currentPosTop - CurrentSlot.slotNumber;
                }
                if (currentPosTop - CurrentSlot.slotNumber == 0)
                {
                    _slotsTop_same.Add(CurrentSlot);
                }
            }
            _slotsTop_same.Sort(CompareDinosBySlot);
            _slotsTop_ubove.Sort(CompareDinosBySlot);
            _slotsTop_under.Sort(CompareDinosBySlotreverse);
            if (undermax < abovemax)
            {
                if (_slotsTop_same.Count > 0)
                {
                    pathTop.AddRange(_slotsTop_same);
                }
                if (_slotsTop_under.Count > 0)
                {
                    pathTop.AddRange(_slotsTop_under);
                }
                if (_slotsTop_ubove.Count > 0)
                {
                    pathTop.AddRange(_slotsTop_ubove);
                }
            }
            else
            {
                if (_slotsTop_same.Count > 0)
                {
                    pathTop.AddRange(_slotsTop_same);
                }
                if (_slotsTop_ubove.Count > 0)
                {
                    pathTop.AddRange(_slotsTop_ubove);
                }
                if (_slotsTop_under.Count > 0)
                {
                    pathTop.AddRange(_slotsTop_under);
                }

            }
           
        }
        public Boolean fetchNextBottom(conveyor conveyorControler)
        {
            Boolean toreturn = false;
            toreturn = conveyorControler.getItem(pathBottom[bottomIndex].slotNumber, true);
            currentlyFetching = pathBottom[bottomIndex];
            if (bottomIndex == pathBottom.Count - 1)
            {
                bottomEnd = true;
            }
            bottomIndex++;
            return toreturn;
        }
        public Boolean fetchNextTop(conveyor conveyorControler)
        {
            Boolean toreturn = false;
            toreturn = conveyorControler.getItem(pathTop[topIndx].slotNumber,false);
            currentlyFetching = pathTop[topIndx];
            if (topIndx == pathTop.Count - 1)
            {
                topEnd = true;
            }
            topIndx++;
            return toreturn;
        }
    }
}
